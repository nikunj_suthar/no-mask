/*-----------------------------------------------------------------
    LOADER
-------------------------------------------------------------------*/
document.onreadystatechange = function () {
	if (document.readyState !== "complete") {
		document.querySelector("body").style.visibility = "hidden";
		document.querySelector("#loader").style.visibility = "visible";
	} else {
		document.querySelector("#loader").style.display = "none";
		document.querySelector("body").style.visibility = "visible";
	}
};

$(document).ready(function () {
	AOS.init();

	$(window).scroll(function () {
		if ($(this).scrollTop() > 500) {
			$("#back-to-top").fadeIn();
		} else {
			$("#back-to-top").fadeOut();
		}
	});
	// scroll body to 0px on click
	$("#back-to-top").click(function () {
		$("body,html").animate(
			{
				scrollTop: 0,
			},
			400
		);
		return false;
	});
});

let tooltipSelector = document.querySelector(".back-to-top");
let tooltip = new bootstrap.Tooltip(tooltipSelector);

/* scroll to a section */
$(".nav-link").each(function () {
	$(this).on("click", () => {
		let targetID = $(this).data("link");
		$("html, body").animate({
			scrollTop: $("#" + targetID).offset().top - 140,
		});
	});
});




// rollnumber
$diy = $('.number-diy-one .data');
$diy_two = $('.number-diy-two .data');
$diy.rollNumber({
  number: $diy[0].dataset.number, 
  speed: 500, 
  interval: 200,
  rooms: 8,
  space: 80,
  //symbol: ',', 
  fontStyle: {
    'font-size': 55,
    'font-family': 'Sailec',
  }
})
$diy_two.rollNumber({
  number: $diy_two[0].dataset.number, 
  speed: 500, 
  interval: 200,
  rooms: 8,
  space: 80,
  //symbol: ',', 
  fontStyle: {
    'font-size': 55,
    'font-family': 'Sailec',
  }
})
  
/* DATA TABLE */
$('#supporter_table').DataTable({
	searching: true,
	responsive: true,
	paging: true,
	info: true
});


/* Validation */
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
	'use strict'
  
	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	var forms = document.querySelectorAll('.needs-validation')
  
	// Loop over them and prevent submission
	Array.prototype.slice.call(forms)
	  .forEach(function (form) {
		form.addEventListener('submit', function (event) {
		  if (!form.checkValidity()) {
			event.preventDefault()
			event.stopPropagation()
		  }
  
		  form.classList.add('was-validated')
		}, false)
	  })
  })()

  /* Checked checkbox show Textbox */
function firmen() {
	var checkBox = document.getElementById("Firmen-oder");
	var text = document.getElementById("Firmen-oder-text");
	if (checkBox.checked == true){
	  text.style.display = "block";
	} else {
	   text.style.display = "none";
	}
}

function postfachfun() {
	var checkBox = document.getElementById("postfach");
	var text = document.getElementById("postfach-text");
	if (checkBox.checked == true){
	  text.style.display = "block";
	} else {
	   text.style.display = "none";
	}
}