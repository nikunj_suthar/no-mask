<?php
require_once('./config.php');
session_start();
if (isset($_POST['submit'])) {
    $anrede = '';
    if (isset($_POST['anrede'])) {
        $anrede = mysqli_real_escape_string($conn, strip_tags($_POST['anrede']));
    }
    $vorname = '';
    if (isset($_POST['vorname'])) {
        $vorname = mysqli_real_escape_string($conn, strip_tags($_POST['vorname']));
    }
    $nachname = '';
    if (isset($_POST['nachname'])) {
        $nachname = mysqli_real_escape_string($conn, strip_tags($_POST['nachname']));
    }
    $firma_organisation = '';
    if (isset($_POST['firma_organisation'])) {
        $firma_organisation = mysqli_real_escape_string($conn, strip_tags($_POST['firma_organisation']));
    }
    $email = '';
    if (isset($_POST['email'])) {
        $email = mysqli_real_escape_string($conn, strip_tags($_POST['email']));
    }
    $telefon = '';
    if (isset($_POST['telefon'])) {
        $telefon = mysqli_real_escape_string($conn, strip_tags($_POST['telefon']));
    }
    $land = '';
    if (isset($_POST['land'])) {
        $land = mysqli_real_escape_string($conn, strip_tags($_POST['land']));
    }
    $strasse = '';
    if (isset($_POST['strasse'])) {
        $strasse = mysqli_real_escape_string($conn, strip_tags($_POST['strasse']));
    }
    $plz = '';
    if (isset($_POST['plz'])) {
        $plz = mysqli_real_escape_string($conn, strip_tags($_POST['plz']));
    }
    $ort = '';
    if (isset($_POST['ort'])) {
        $ort = mysqli_real_escape_string($conn, strip_tags($_POST['ort']));
    }
    $postfach = '';
    if (isset($_POST['postfach'])) {
        $postfach = mysqli_real_escape_string($conn, strip_tags($_POST['postfach']));
    }
    $term_condition = '';
    if (isset($_POST['term_condition'])) {
        $term_condition = mysqli_real_escape_string($conn, strip_tags($_POST['term_condition']));
    }


    // $sql = "INSERT INTO support_mst ('anrede','vorname','nachname','firma_organisation','email','telefon','land','strasse','plz','ort','postfach','term_condition') VALUES ($anrede,$vorname,$nachname,$firma_organisation,$email,$telefon,$land,$strasse,$plz,$ort,$postfach,$term_condition)" ;
    // if (mysqli_query ($conn , $sql) ) {
    //     $msg = "Successfully Created"; 
    // }else {
    //     $msg = "oops! There is an error when creating your record. Retry again"; 

    // } 
    if ($stmt = mysqli_prepare($conn, "INSERT INTO support_mst SET anrede=?, vorname=?, nachname=?, firma_organisation=?,email=?,telefon=?,land=?,strasse=?,plz=?,ort=?,postfach=?,term_condition=?")) {
        mysqli_stmt_bind_param($stmt, "ssssssssssss", $anrede, $vorname, $nachname, $firma_organisation, $email, $telefon, $land, $strasse, $plz, $ort, $postfach, $term_condition);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        $_SESSION["success_msg"] = "Thank you for filling out your information!";
    } else {
        $_SESSION["error_msg"] = "Oops. Something went wrong. Please try again later.";
    }
    header("location: ./");
    exit();
}
?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>No Mask</title>
    <link rel="shortcut icon" href="./img/favicon.png" type="image/x-icon">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.2.0/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <link rel="stylesheet" href="./css/custom.css">
</head>

<body>
    <div id="loader">
        <span class="loading"></span>
    </div>

    <header class="sticky-top" style="z-index: 1040;">
        <nav class="py-2 py-lg-3 pb-lg-3 navbar navbar-expand-xxl flex-column">
            <div class="container-xxl mt-2 mt-lg-4">
                <div class="d-block d-xxl-none">
                    <a href="#paymentModal" data-bs-toggle="modal" class="btn btn-lg btn-secondary-gradient nav-link py-lg-3 rounded-pill text-white">
                        <i class="fa fa-heart"></i> JETZT UNTERSTÜTZEN
                    </a>
                </div>
                <button class="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <i class="fas fa-bars"></i>
                    </span>
                </button>
                <div class="col-12 col-xxl-auto">
                    <div class="navigation">
                        <div class="py-lg-2 px-xl-4 collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-lg-3 mb-2 mb-lg-0 ">
                                <li class="nav-item dropdown py-1  py-lg-0 px-3 px-lg-0">
                                    <a class="nav-link link-scroll" href="javascript:void(0);" data-link="mandatoryMask">MASKENPFLICHT FÜR KINDER</a>
                                </li>
                                <li class="nav-item dropdown py-1  py-lg-0 px-3 px-lg-0">
                                    <a class="nav-link link-scroll" href="javascript:void(0);" data-link="weClarify">WIR KLÄREN AUF</a>
                                </li>
                                <li class="nav-item dropdown py-1  py-lg-0 px-3 px-lg-0">
                                    <a class="nav-link link-scroll" href="javascript:void(0);" data-link="theFilm">DER FILM</a>
                                </li>
                                <li class="nav-item dropdown py-1  py-lg-0 px-3 px-lg-0">
                                    <a class="nav-link link-scroll" href="javascript:void(0);" data-link="theClaim">DIE KLAGE</a>
                                </li>
                                <li class="nav-item dropdown py-1  py-lg-0 px-3 px-lg-0">
                                    <a class="nav-link link-scroll" href="javascript:void(0);" data-link="legalRepresentation">RECHTSVERTRETUNG</a>
                                </li>
                                <li class="nav-item dropdown py-1  py-lg-0 px-3 px-lg-0">
                                    <a class="nav-link link-scroll" href="javascript:void(0);" data-link="participants">TEILNEHMER</a>
                                </li>
                                <li class="nav-item dropdown py-1  py-lg-0 px-3 px-lg-0">
                                    <a class="nav-link link-scroll" href="javascript:void(0);" data-link="updates">UPDATES</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="d-none d-xxl-block">
                    <a href="#paymentModal" data-bs-toggle="modal" class="btn btn-lg btn-secondary-gradient nav-link py-lg-3 rounded-pill text-white">
                        <i class="fa fa-heart"></i> JETZT UNTERSTÜTZEN
                    </a>
                </div>
            </div>
        </nav>
    </header>

    <main class="overflow-hidden">
        <section id="mandatoryMask" class="position-relative section-zig-zag pt-lg-5">
            <h2 class="h3 text-secondary text-center" data-aos="fade-up">!! SAMMELKLAGE GEGEN DIE MASKENPFLICHT !!</h2>
            <img src="./img/banner-01.png" alt="" class="img-fluid section-img-right">
            <div class="container-xxl">
                <div class="row">
                    <div class="col-lg-5 pt-xl-5 mt-xl-5">
                        <h1 class="h1 title-border" data-aos="fade-up">Masken sind für <span>Kinder schädlich</span>
                        </h1>
                        <p class="letter-space">Im Unterricht und auf Schulhöfen in Liechtenstein herrscht jetzt Maskenpflicht, mit der man der Ausbreitung des Coronavirus SARS-CoV2 und all seinen Varianten Herr werden möchte. Viele Experten sind jedoch der Meinung, dass das Tragen des Mund-Nasen-Schutzes für Kinder eine Gesundheitsgefährdung darstellt!</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="position-relative section-zig-zag pt-lg-5">
            <img src="./img/banner-02.png" alt="" class="img-fluid section-img-left mt-n9">
            <div class="container-xxl">
                <div class="row">
                    <div class="offset-lg-5 col-lg-7 pt-xl-5 mt-xl-5 text-lg-end">
                        <h1 class="h2 title-border" data-aos="fade-up">Gesundheitsprobleme durch <span>Maskenpflicht für
                                Kinder</span></h1>
                        <p class="letter-space">In Liechtenstein müssen jetzt Kinder ab 6 Jahren im Unterricht bzw. im Kindesalter Masken bzw. einen sog. Mund-Nasen-Schutz tragen - und das obwohl die Beweislage für einen Nutzen nicht überzeugend ist (<a class="text-secondary" href="https://jamanetwork.com/journals/jamapediatrics/fullarticle/2781743" target="_blank" rel="noopener noreferrer"> 1 </a>), denn Kinder erkranken so gut wie nicht an Covid-19 und wenn, so nur sehr milde. Und selbst wenn Kinder überhaupt keine Symptome zeigten, hatten sie - nach einem SARS-CoV-2-Kontakt - länger Antikörper im Blut als Erwachsene, so eine Studie der Uni Freiburg vom Juli 2021 (<a class="text-secondary" href="https://www.medrxiv.org/content/10.1101/2021.07.20.21260863v1" target="_blank" rel="noopener noreferrer"> 9 </a>). </p>
                        <p class="letter-space">Selbst Kinder mit einschlägigen Vorerkrankungen haben zwar ein erhöhtes Risiko, an Covid-19 zu erkranken (in den Jahren zuvor an Grippe), doch kommt es so gut wie nie zu Todesfällen (<a class="text-secondary" href="https://www.medrxiv.org/content/10.1101/2021.07.01.21259785v1" target="_blank" rel="noopener noreferrer"> 6 </a>) (<a class="text-secondary" href="https://www.researchsquare.com/article/rs-689684/v1" target="_blank" rel="noopener noreferrer"> 7 </a>) (<a class="text-secondary" href="https://www.medrxiv.org/content/10.1101/2021.06.30.21259763v1" target="_blank" rel="noopener noreferrer"> 8 </a>).</p>
                        <p class="letter-space">Masken nützen bei Kindern also nichts, ja, sie führen sogar zu gesundheitlichen Beeinträchtigungen! In einer Umfrage, deren Auswertung im Frühjahr 2021 erschienen war (<a class="text-secondary" href="https://www.researchsquare.com/article/rs-124394/v1" target="_blank" rel="noopener noreferrer"> 2 </a>), zeigten die Daten von 25.930 Kindern, dass 68 Prozent der Kinder durch den Mund-Nasen-Schutz gesundheitliche Probleme hatten. Sie litten an Kopfschmerzen, Müdigkeit, Erschöpfung und schlechter Stimmung. Als Grund wird u. a. ein zu hoher Kohlendioxidgehalt der Atemluft hinter der Maske angegeben.</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="weClarify" class="we-clarify">
            <h2 class="h2 text-center title-border" data-aos="fade-up"><span>WIR KLÄREN AUF</span></h2>
            <p class="title-border text-center">!! Masken <span>Risiko</span> und <span>Fakten</span> !!</p>
            <div class="clarify-detail-wrap rounded-pill">
                <div class="container-xxl mt-5">
                    <div class="row justify-content-center gx-md-5">
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/01.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Kohlendioxid-Grenzwerte werden weit überschritten</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Kohlendioxid-Grenzwerte-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/02.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Bedenkliche Werte schon nach einer Minute</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Bedenkliche-Werte-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/03.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Kindermasken lösen das Problem nicht</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Kindermasken-lösen-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>

                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/04.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Verharmlosung durch Faktenchecker</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Verharmlosung-durch-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/05.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Kein Unterschied zwischen OP-Masken und FFP2-Masken</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Kein-Unterschied-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/06.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Schulen dürfen keine Untersuchungen durchführen lassen</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Schulen-dürfen-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/07.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Masken schaden mehr, als sie nützen</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Masken-schaden-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/08.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Unter 16 Jahren sind Masken für Kinder nicht sinnvoll</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Unter-16-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/09.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Seelische Schäden bei Kindern durch Masken</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Seelische-Schäden-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/10.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Übertragungsrisiko durch Kinder ist minimal</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Übertragungsrisiko-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/11.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Keine Maskenpause für Kinder</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Keine-Maskenpause-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="d-none d-lg-block"></div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/12.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Masken verhindern Entwicklung des Atemtrakts bei Kindern</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Masken-verhindern-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <figure class="text-center text-white">
                                <img src="./img/13.png" alt="" class="img-fluid rounded-circle mb-3">
                                <p>Social Distancing und Angst schwächen das Immunsystem</p>
                                <a class="text-secondary" data-bs-toggle="modal" href="#Social-Distancing-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="theFilm" class="the-movie">
            <div class="container">
                <h2 class="mt-5 h2 text-center title-border mb-3" data-aos="fade-up"><span>Kinder Mit Maske - Der Film</span></h2>
                <p class="text-center">In der Pandemie sind die Kinder die stillen Leidtragenden. Ihre Bedürfnisse werden nicht gesehen. Dabei brauchen sie Gesichter, Kontakt, Bewegung. Statt es ihnen zu ermöglichen, wird es genommen. Die Folgen der Corona-Massnahmen werden unterschätzt und verharmlost. In dem 45-Minuten-Interview-Film „Corona Kinder“ von Jens-Tibor Homm, kommen Ärzte, Psychologen, Lehrer und Wissenschaftler aus verschiedensten Bereichen zu Wort und geben Antworten.</p>
                <div class="row justify-content-center mt-3 mt-lg-5" style="background: url(./img/wave-line.png)no-repeat center / contain;">
                    <div class="col-12 col-md-9">
                        <div class="ratio ratio-16x9">
                            <video poster="./img/video-cover.png" controls>
                                <source src="./img/Corona-Kinder-Der-Film.mp4" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-3 py-lg-5 experts">
            <h4 class="h2 title-border text-center mb-5" data-aos="fade-up"><span>Die Experten</span></h4>
            <div class="container">
                <div class="row justify-content-center gx-lg-5">
                    <div class="col-6 col-md-4 col-lg-3 mb-xl-4 text-center">
                        <figure class="img-expert">
                            <img src="./img/dr-01.png" alt="">
                        </figure>
                        <p>Dr. med. Michaela Glöckler</p>
                        <a class="text-secondary" data-bs-toggle="modal" href="#Michaela-Glöckler-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 mb-xl-4 text-center">
                        <figure class="img-expert">
                            <img src="./img/dr-02.png" alt="">
                        </figure>
                        <p>Dr. med. Steffen Rabe</p>
                        <a class="text-secondary" data-bs-toggle="modal" href="#Steffen-Rabe-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 mb-xl-4 text-center">
                        <figure class="img-expert">
                            <img src="./img/dr-03.png" alt="">
                        </figure>
                        <p class="text-center">Prof. Dr. med. Dr. rer. nat. M.Sc. Christian Schubert</p>
                        <a class="text-secondary" data-bs-toggle="modal" href="#Christian-Schubert-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 mb-xl-4 text-center">
                        <figure class="img-expert">
                            <img src="./img/dr-04.png" alt="">
                        </figure>
                        <p>Prof. Dr. Franz Ruppert,</p>
                        <a class="text-secondary" data-bs-toggle="modal" href="#Franz-Ruppert-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 mb-xl-4 text-center">
                        <figure class="img-expert">
                            <img src="./img/dr-05.png" alt="">
                        </figure>
                        <p>Hans-Christian Prestien</p>
                        <a class="text-secondary" data-bs-toggle="modal" href="#Christian-Prestien-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 mb-xl-4 text-center">
                        <figure class="img-expert">
                            <img src="./img/dr-06.png" alt="">
                        </figure>
                        <p>Prof. Dr. Christian KreiB</p>
                        <a class="text-secondary" data-bs-toggle="modal" href="#Christian-KreiB-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 mb-xl-4 text-center">
                        <figure class="img-expert">
                            <img src="./img/dr-07.png" alt="">
                        </figure>
                        <p>Leonard Heffels</p>
                        <a class="text-secondary" data-bs-toggle="modal" href="#Leonard-Heffels-modal">Mehr Erfahren <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="d-none d-md-block" style="height: 5rem;"></div>
        </section>
        <section class="py-3 py-lg-5">
            <div class="container">
                <div class="row gx-lg-5">
                    <div class="col-md-9 col-lg-8">
                        <h4 class="h2 title-border mb-3" data-aos="fade-up"><span>Der Filmemacher</span></h4>
                        <p>Jens-Tibor Homm, ist Kameramann und Filmemacher. Seine Leidenschaft ist es, Menschen zu beobachten. Mit 13 Jahren begann seine Filmbegeisterung. Er produzierte etliche Kurzfilme, darunter der Film „Ein Schwarz Weiss Film“, der zahlreiche Preise auf nationalen und internationalen Filmfestivals gewann. Seine Faszination von Menschen und deren Geschichten war der Entschluss in Richtung Dokumentarfilm zu gehen. 2010 produzierte er gleich zwei Dokumentarfilme. Inzwischen arbeitet er als Regie-Kameramann im Dokumentarfilm.</p>
                    </div>
                    <div class="col-md-3 col-lg-4">
                        <img src="./img/film-maker.png" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>
        <section id="theClaim" class="py-3 py-lg-5">
            <div class="container-xxl my-3 complaint">
                <div class="border border-white border-radius-3">
                    <div class="row">
                        <div class="col-md-4 col-lg-5">
                            <img src="./img/law.png" alt="" class="img-fluid border-radius-3">
                        </div>
                        <div class="col-md-8 col-lg-7 text-md-end align-self-center">
                            <div class="p-4">
                                <h4 class="h2 title-border mb-5" data-aos="fade-up"><span>Die Klage</span></h4>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
                                    piece of classical Latin literature from 45 BC, making it over 2000 years old.
                                    Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked
                                    up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and
                                    going through the cites of the word in classical literature, discovered the
                                    undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de
                                    Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45
                                    BC. This book is a treatise on the theory of ethics, very popular during the
                                    Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes
                                    from a line in section 1.10.32.</p>
                                <button type="button" class="btn btn-secondary btn-lg">Read more</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="legalRepresentation" class="py-3 py-lg-5 mt-lg-5" style="background: url('./img/bg-shape-01.png')no-repeat top center / cover;">
            <div class="container-xxl legal-representation">
                <div class="row">
                    <div class="col-lg-4">
                        <h4 class="h2 title-border mb-3" data-aos="fade-up"><span>Rechtsvertretung</span></h4>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                    </div>
                </div>
                <div class="row representation-person">
                    <div class="col-lg-4" data-aos="fade-up" data-aos-delay="600">
                        <div class="row gx-0 align-items-end">
                            <div class="col-lg-6">
                                <img src="./img/lawyer_01.png" alt="" class="img-fluid">
                            </div>
                            <div class="col-lg-6">
                                <div class="bg-white p-4 p-md-4 border border-2 border-secondary text-primary border-radius-3 mb-lg-5">
                                    <ul class="list-unstyled">
                                        <li>lic. iur.</li>
                                        <li class="text-secondary h4">Gert Schuster</li>
                                        <li>LL.M.,</li>
                                        <li>Loreum ipsum, Partner</li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li><a href="mailto:loreum.ipsum@xyz.com">loreum.ipsum@xyz.com</a></li>
                                        <li>T <a href="tel:+0000000000">+000 000 00 00</a></li>
                                        <li>F <a href="fax:+0000000000">+000 000 00 00</a></li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li>Kontakt <a href="#" class="text-secondary">vCARD</a> | <a href="#" class="text-secondary">CV (PDF)</a></li>
                                    </ul>

                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#" class="text-secondary">Details erfahren</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4" data-aos="fade-up" data-aos-delay="300">
                        <div class="row gx-0 align-items-end">
                            <div class="col-lg-6">
                                <img src="./img/lawyer_02.png" alt="" class="img-fluid">
                            </div>
                            <div class="col-lg-6">
                                <div class="bg-white p-4 p-md-4 border border-2 border-secondary text-primary border-radius-3 mb-lg-5">
                                    <ul class="list-unstyled">
                                        <li class="text-secondary h4">Vreni Dirksen</li>
                                        <li>MLaw,</li>
                                        <li>Loreum ipsum</li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li><a href="mailto:loreum.ipsum@xyz.com">loreum.ipsum@xyz.com</a></li>
                                        <li>T <a href="tel:+0000000000">+000 000 00 00</a></li>
                                        <li>F <a href="fax:+0000000000">+000 000 00 00</a></li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li>Kontakt <a href="#" class="text-secondary">vCARD</a> | <a href="#" class="text-secondary">CV (PDF)</a></li>
                                    </ul>

                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#" class="text-secondary">Details erfahren</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4" data-aos="fade-up">
                        <div class="row gx-0 align-items-end">
                            <div class="col-lg-6">
                                <img src="./img/lawyer_03.png" alt="" class="img-fluid">
                            </div>
                            <div class="col-lg-6">
                                <div class="bg-white p-4 p-md-4 border border-2 border-secondary text-primary border-radius-3 mb-lg-5">
                                    <ul class="list-unstyled">
                                        <li class="text-secondary h4">Adala Schmitt</li>
                                        <li>MLaw,</li>
                                        <li>Loreum ipsum</li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li><a href="mailto:loreum.ipsum@xyz.com">loreum.ipsum@xyz.com</a></li>
                                        <li>T <a href="tel:+0000000000">+000 000 00 00</a></li>
                                        <li>F <a href="fax:+0000000000">+000 000 00 00</a></li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li>Kontakt <a href="#" class="text-secondary">vCARD</a> | <a href="#" class="text-secondary">CV (PDF)</a></li>
                                    </ul>

                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#" class="text-secondary">Details erfahren</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-xxl mb-5 counter">
                <h4 class="h2 text-center title-border mb-5" data-aos="fade-up"><span>Jetzt Unterstutzen</span></h4>
                <div class="d-none d-md-block" style="height: 60px"></div>
                <div class="row gx-md-5 pb-md-5">
                    <div class="col-md-6">
                        <div class="bg-white border-radius-2 p-3 mb-5 mb-lg-0">
                            <div class="mt-n3 text-center">
                                <span class="text-white h3 px-4 position-relative d-inline-block bg-secondary rounded-pill p-2 mb-4">Aktuelle Summe</span>
                            </div>
                            <h6 class="h3 text-primary text-center text-uppercase">Spendenzahler</h6>
                            <div class="number-diy-one">
                                <div class="data" data-number="85236978"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="bg-white border-radius-2 p-3 mb-4 mb-lg-0">
                            <div class="mt-n3 text-center">
                                <span class="text-white h3 px-4 position-relative d-inline-block bg-secondary rounded-pill p-2 mb-4">Zielsumme</span>
                            </div>
                            <h6 class="h3 text-primary text-center text-uppercase">Spendenzahler</h6>
                            <div class="number-diy-two">
                                <div class="data" data-number="96328712"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <p>Jeder Franken zählt, egal ob Unternehmen, Privatperson oder Eltern, wenn auch Sie der Meinung sind, dass die Maskenpflicht für Kleinkinder zu weit geht, dann unterstützen Sie uns JETZT und HIER!</p>
                        <p>Sie blieben bisher inaktiv, weil Sie der Meinung gewesen sind, dass Sie gegen die diktierten Massnahmen sowieso machtlos sind und nichts dagegen unternehmen können?</p>
                        <p>Jetzt ist der Moment da, bei welchem Sie aktiv zur Veränderung beitragen können.</p>
                        <p>Gerichtsverhandlungen und Anwälte sind kostspielig, wir können diese Summen nur gemeinsam Stämmen. </p>
                        <p>! Das Ziel sind 150'000 Schweizerfranken !</p>
                    </div>
                </div>
            </div>

            <h4 class="h2 text-center title-border my-5" data-aos="fade-up"><span>Meilensteine</span></h4>
            <div class="d-none d-md-block" style="height: 40px"></div>
            <div class="milestones rounded-pill">
                <img src="./img/arrow-down.png" alt="" class="d-block mx-auto img-fluid mb-5">
                <div class="container">
                    <div class="timeline">
                        <div class="row align-items-center no-gutters justify-content-end justify-content-md-around align-items-start  timeline-nodes">
                            <div class="col-8 col-md-5 order-3 order-md-1 timeline-content">
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum .</p>
                            </div>
                            <div class="col-4 col-sm-2 px-md-3 order-2 timeline-image text-md-center">
                                <div class="ratio ratio-1x1 rounded-circle border border-2 border-secondary bg-secondary text-white">
                                    <div class="align-items-center d-flex flex-column justify-content-center">
                                        <i class="fa fa-check fa-3x my-1 my-md-2"></i>
                                        <h3 class="h3 mb-0 text-center fw-bold">CHF<br />20'000</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8 col-md-5 order-1 order-md-3 timeline-date">
                                <h5 class="h2">Lorem Ipsum</h5>
                            </div>
                        </div>
                        <div class="row align-items-center no-gutters justify-content-end justify-content-md-around align-items-start  timeline-nodes">
                            <div class="col-8 col-md-5 order-3 order-md-1 timeline-content">
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum .</p>
                            </div>
                            <div class="col-4 col-sm-2 px-md-3 order-2 timeline-image text-md-center">
                                <div class="ratio ratio-1x1 rounded-circle border border-2 border-secondary bg-secondary text-white">
                                    <div class="align-items-center d-flex flex-column justify-content-center">
                                        <i class="fa fa-check fa-3x my-2"></i>
                                        <h3 class="h3 mb-0 text-center fw-bold">CHF<br />20'000</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8 col-md-5 order-1 order-md-3 timeline-date">
                                <h5 class="h2">Lorem Ipsum</h5>
                            </div>
                        </div>
                        <div class="row align-items-center no-gutters justify-content-end justify-content-md-around align-items-start  timeline-nodes">
                            <div class="col-8 col-md-5 order-3 order-md-1 timeline-content">
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum .</p>
                            </div>
                            <div class="col-4 col-sm-2 px-md-3 order-2 timeline-image text-md-center">
                                <div class="ratio ratio-1x1 rounded-circle border border-2 border-secondary bg-secondary text-white">
                                    <div class="align-items-center d-flex flex-column justify-content-center">
                                        <i class="fa fa-check fa-3x my-2"></i>
                                        <h3 class="h3 mb-0 text-center fw-bold">CHF<br />20'000</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8 col-md-5 order-1 order-md-3 timeline-date">
                                <h5 class="h2">Lorem Ipsum</h5>
                            </div>
                        </div>
                        <div class="row align-items-center no-gutters justify-content-end justify-content-md-around align-items-start  timeline-nodes">
                            <div class="col-8 col-md-5 order-3 order-md-1 timeline-content">
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum .</p>
                            </div>
                            <div class="col-4 col-sm-2 px-md-3 order-2 timeline-image text-md-center">
                                <div class="ratio ratio-1x1 rounded-circle border border-2 border-secondary bg-secondary text-white">
                                    <div class="align-items-center d-flex flex-column justify-content-center">
                                        <i class="fa fa-check fa-3x my-2"></i>
                                        <h3 class="h3 mb-0 text-center fw-bold">CHF<br />20'000</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8 col-md-5 order-1 order-md-3 timeline-date">
                                <h5 class="h2">Lorem Ipsum</h5>
                            </div>
                        </div>
                        <div class="row align-items-center no-gutters justify-content-end justify-content-md-around align-items-start  timeline-nodes">
                            <div class="col-8 col-md-5 order-3 order-md-1 timeline-content">
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum .</p>
                            </div>
                            <div class="col-4 col-sm-2 px-md-3 order-2 timeline-image text-md-center">
                                <div class="ratio ratio-1x1 rounded-circle border border-2 border-secondary bg-secondary text-white">
                                    <div class="align-items-center d-flex flex-column justify-content-center">
                                        <i class="fa fa-check fa-3x my-2"></i>
                                        <h3 class="h3 mb-0 text-center fw-bold">CHF<br />20'000</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8 col-md-5 order-1 order-md-3 timeline-date">
                                <h5 class="h2">Lorem Ipsum</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="d-none d-lg-block" style="height: 12rem;"></div>

        <section class="donation mt-lg-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 offset-lg-6">
                        <div class="card border-radius-2 border-0 overflow-hidden">
                            <h5 class="h3 card-title text-center gradient-secondary mb-0" data-aos="fade-up">Allegemeine Spende</h5>
                            <div class="card-body p-lg-4 text-primary pb-md-5" data-aos="fade-left">
                                <form action="" method="post">
                                    <div class="form-check mb-3">
                                        <input class="form-check-input me-3" type="radio" name="Spende" id="opt1">
                                        <label class="form-check-label" for="opt1">Ich mochte anonym spenden</label>
                                    </div>
                                    <div class="form-check mb-3">
                                        <input class="form-check-input me-3" type="radio" name="Spende" id="opt2" checked>
                                        <label class="form-check-label" for="opt2">Ich monchte als Privatperson spenden (Sichtbar)</label>
                                    </div>
                                    <div class="form-check mb-3">
                                        <input class="form-check-input me-3" type="radio" name="Spende" id="opt3">
                                        <label class="form-check-label" for="opt3">Ich mochte als Unternehmen spenden (Sichtbar)</label>
                                    </div>

                                    <h5 class="h4 my-4">Spendenbetrag:</h5>
                                    <div class="row gx-5 mb-2">
                                        <div class="col-sm-6 mb-4">
                                            <div class="custom-radio position-relative">
                                                <input class="form-check-input m-0" type="radio" name="donation_amount_parent" id="donation_amount_1">
                                                <div class="border border-1 border-primary p-3 text-primary align-items-center justify-content-around d-flex">
                                                    <span class="circle"></span><span class="h3 fw-bold m-0">20</span><span>CHF</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mb-4">
                                            <div class="custom-radio position-relative">
                                                <input class="form-check-input m-0" type="radio" name="donation_amount_parent" id="donation_amount_1">
                                                <div class="border border-1 border-primary p-3 text-primary align-items-center justify-content-around d-flex">
                                                    <span class="circle"></span><span class="h3 fw-bold m-0">40</span><span>CHF</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mb-4">
                                            <div class="custom-radio position-relative">
                                                <input class="form-check-input m-0" type="radio" name="donation_amount_parent" id="donation_amount_1">
                                                <div class="border border-1 border-primary p-3 text-primary align-items-center justify-content-around d-flex">
                                                    <span class="circle"></span><span class="h3 fw-bold m-0">60</span><span>CHF</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mb-4">
                                            <div class="custom-radio position-relative">
                                                <input class="form-check-input m-0" type="radio" name="donation_amount_parent" id="donation_amount_1">
                                                <div class="border border-1 border-primary p-3 text-primary align-items-center justify-content-around d-flex">
                                                    <span class="circle"></span><input placeholder="Custom Amount" type="number" name="" id=""><span>CHF</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-check mb-3">
                                        <input class="form-check-input me-3" type="radio" name="agree" id="agree_term">
                                        <label class="form-check-label" for="agree_term">Loreum ipsum, Loreum ipsum Loreum ipsum</label>
                                    </div>
                                    <div class="align-items-center d-flex flex-column justify-content-center">
                                        <button type="submit" class="btn py-lg-3 border-radius-2 btn-lg btn-secondary-gradient text-white h3">JETZT UNTERSTÜTZEN</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="participants" class="mt-3 mt-lg-5 py-4 supporters">
            <h4 class="h2 text-center title-border my-2 my-lg-5" data-aos="fade-up"><span>Danke an alle unterstutzer</span></h4>
            <div class="container">
                <div class="row mb-5 justify-content-between">
                    <div class="col-md-auto">
                        <div class="d-flex flex-fill align-items-center">
                            <img src="./img/company.svg" alt="" class="img-fluid">
                            <h4 class="h3 m-0 ms-3">Unternehmen</h4>
                        </div>
                    </div>
                    <div class="col-md-auto">
                        <div class="d-flex flex-fill align-items-center">
                            <img src="./img/Private-individuals.svg" alt="" class="img-fluid">
                            <h4 class="h3 m-0 ms-3">Privatpersonen</h4>
                        </div>
                    </div>
                    <div class="col-md-auto">
                        <div class="d-flex flex-fill align-items-center">
                            <img src="./img/Anonymous.svg" alt="" class="img-fluid">
                            <h4 class="h3 m-0 ms-3">Anonym</h4>
                        </div>
                    </div>
                </div>
                <table id="supporter_table" class="table align-middle text-primary bg-white nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Datum</th>
                            <th>Betrag</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img src="./img/company-icon.svg" alt="" class="me-2">Applie AG</td>
                            <td>17.12.2021</td>
                            <td>CHF 20'000</td>
                        </tr>
                        <tr>
                            <td><img src="./img/private-icon.svg" alt="" class="me-2">Wolfinger Fabio</td>
                            <td>17.12.2021</td>
                            <td>CHF 20'000</td>
                        </tr>
                        <tr>
                            <td><img src="./img/anonymous-icon.svg" alt="" class="me-2">anonymous</td>
                            <td>17.12.2021</td>
                            <td>CHF 20'000</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>

        <section id="updates" class="updates py-4">
            <div class="container">
                <h4 class="h2 title-border mt-5" data-aos="fade-up"><span>Updates</span></h4>
                <p>Lorem Ipsum is simply dummy text of the printing<br class="d-none d-lg-block" />
                    and typesetting industry.</p>
                <div class="row gx-5">
                    <div class="col-lg-6">
                        <figure>
                            <img src="./img/updates.png" alt="" class="img-fluid border-radius-2">
                        </figure>
                        <h4 class="h3">Loreum ipsum Loreum?</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="col-lg-6">
                        <div class="row gx-lg-5 flex-lg-row-reverse mb-5">
                            <div class="col-md-3 col-lg-6">
                                <img src="./img/updates.png" alt="" class="img-fluid border-radius-2 mb-3 mb-lg-0">
                            </div>
                            <div class="col-md-9 col-lg-6 d-flex flex-column flex-fill">
                                <div>
                                    <h4 class="h4">Loreum ipsum Loreum?</h4>
                                    <p class="small">With so many crypto assets out there, how do you tell once crypto assets from another? Here, we give an example of how to classify.....</p>
                                </div>
                                <small class="mt-auto">June 1, 2021</small>
                            </div>
                        </div>
                        <div class="row gx-lg-5 flex-lg-row-reverse mb-5">
                            <div class="col-md-3 col-lg-6">
                                <img src="./img/updates.png" alt="" class="img-fluid border-radius-2 mb-3 mb-lg-0">
                            </div>
                            <div class="col-md-9 col-lg-6 d-flex flex-column flex-fill">
                                <div>
                                    <h4 class="h4">Loreum ipsum Loreum?</h4>
                                    <p class="small">With so many crypto assets out there, how do you tell once crypto assets from another? Here, we give an example of how to classify.....</p>
                                </div>
                                <small class="mt-auto">June 1, 2021</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <footer>
        <div class="container-xxl">
            <div class="row">
                <div class="col-md-4 py-4 pt-5">
                    <ul class="footer-nav list-unstyled text-center text-md-start">
                        <li><a href="javascript:void(0);" data-link="mandatoryMask" class="nav-link text-uppercase">MASKENPFLICHT FÜR KINDER</a></li>
                        <li><a href="javascript:void(0);" data-link="weClarify" class="nav-link text-uppercase">WIR KLÄREN AUF</a></li>
                        <li><a href="javascript:void(0);" data-link="theFilm" class="nav-link text-uppercase">DER FILM</a></li>
                        <li><a href="javascript:void(0);" data-link="theClaim" class="nav-link text-uppercase">DIE KLAGE</a></li>
                        <li><a href="javascript:void(0);" data-link="legalRepresentation" class="nav-link text-uppercase">RECHTSVERTRETUNG</a></li>
                        <li><a href="javascript:void(0);" data-link="participants" class="nav-link text-uppercase">TEILNEHMER</a></li>
                        <li><a href="javascript:void(0);" data-link="updates" class="nav-link text-uppercase">UPDATES</a></li>
                    </ul>
                </div>
                <div class="col-md-4 py-4 pt-5 contact text-center text-md-start">
                    <ul class="list-unstyled">
                        <li class="h4">Contact</li>
                        <li class="mb-2">43252 Borer Mountains</li>
                        <li class="mb-2">Zackerychester</li>
                        <li class="mb-2">Bahamas</li>
                        <li>123-456-7890</li>
                    </ul>
                </div>
                <div class="col-md-4 py-4 pt-5 ps-md-5 text-center text-md-start">
                    <ul class="list-unstyled">
                        <li class="h4">Newsletters</li>
                        <li class="mb-2">Subscribe our newsletter to get
                            more updates of crypto.</li>
                    </ul>
                    <div class="input-group mb-3 newsletter">
                        <input type="email" class="form-control rounded-pill" placeholder="Enter your email">
                        <button class="btn btn-secondary rounded-circle" type="button">
                            <i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                    <div class="h4 mb-3">Community</div>
                    <ul class="list-unstyled social-icon">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="py-3 border-top border-white text-center">
            <span>© 2021 Applie AG All rights reserved.</span>
        </div>
    </footer>


    <button id="back-to-top" type="button" class="btn btn-secondary text-light rounded-circle btn-lg back-to-top" title="Back on top">
        <i class="fas fa-angle-up"></i>
    </button>

    <!-- WE CLARIFY Modals  -->
    <div class="modal fade" id="Kohlendioxid-Grenzwerte-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Kohlendioxid-Grenzwerte werden weit überschritten</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Der normale Kohlendioxidgehalt im Freien beträgt etwa 400 ppm. Für geschlossene Räume setzte das Bundesumweltamt einen Grenzwert von 2000 ppm fest. Jeder Wert, der darüber liegt, sei nicht akzeptabel, so das Bundesumweltamt schon im Jahr 2008.</p>
                    <p>In einer deutschen Studie nun, die am 30. Juni 2021 in der Fachzeitschrift JAMA Pediatrics der American Medical Association als Research Letter (ohne Review) veröffentlicht wurde und vom Immunologen und Toxikologen Prof. Dr. Stefan Hockertz auf den Weg gebracht sowie vom klinischen Psychologen Prof. Dr. Harald Walach geleitet wurde ( <a class="text-secondary" href="https://jamanetwork.com/journals/jamapediatrics/fullarticle/2781743" target="_blank" rel="noopener noreferrer">1</a> ), zeigte sich, dass der Grenzwert (auch Gefährdungsgrenze genannt) des Bundesumweltamtes für Innenräume unter einer Maske schon nach 3 Minuten fast um das 7-Fache überschritten wurde und Werte von über 13.000 ppm erreichte. TeilnehmerInnen waren 45 Kinder und Jugendliche zwischen 6 und 17 Jahren. (Beachten Sie dazu das Update am Ende des Artikels!) </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Bedenkliche-Werte-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Bedenkliche Werte schon nach einer Minute</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Die Untersuchung wurde aufgrund einer Elterninitiative durchgeführt. Die Eltern hatten sich um ihre Kinder gesorgt, da diese seit der Maskenpflicht über die weiter oben genannten Probleme geklagt hatten. Zwei Ärzte (Andreas Diemer und Ronald Weikl) sowie eine Kinderpsychotherapeuthin (Anna Kappes) begleiteten die Studie medizinisch.</p>
                    <p>Die Messungen führte Helmut Traindl, promovierter Ingenieur aus Wien, durch. Traindl verfügt mit Messungen dieser Art über langjährige Erfahrung. Traindl erklärt:</p>
                    <p>„Die Kinder erhielten einen kleinen Messschlauch in Nasennähe befestigt, so dass wir den Kohlendioxidgehalt in 15 Sekunden-Abständen sauber bestimmen konnten."</p>
                    <p>Zunächst wurden die Kohlendioxidwerte in der Einatemluft ohne Maske gemessen, dann in zufälliger Reihenfolge mit einer OP-Maske und einer FFP2-Maske, anschliessend noch einmal ohne Maske. „Es erstaunte mich, dass die Werte so rasch, nämlich bereits nach 1 Minute so hoch anstiegen und ohne grosse Schwankungen auf hohem Niveau stabil verweilten“, </p>
                    <p>sagt Dr. Traindl in der Pressemitteilung der Ärzte, die auf der Website der Stiftung Corona Ausschuss 2020News zu finden ist und auch gemeinsam mit der Originalstudie die Quelle nachfolgender Informationen ist (3).</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Kindermasken-lösen-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Kindermasken lösen das Problem nicht</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Die Erklärung für den steigenden Kohlendioxidgehalt der Einatemluft liefert Andreas Diemer: „Der Totraum in der Maske ist für Kinder im Verhältnis zu ihrem Gesicht besonders gross. Dort sammelt sich das ausgeatmete Kohlendioxid, mischt sich mit der einströmenden Luft und wird rückgeatmet. Weil bei Kindern der Atemvorgang schneller geht und auch weniger Druck erzeugt, ist gerade bei ihnen das Problem des mangelnden Gas-Austausches besonders gross. Auch Kindermasken lösen das Problem nicht. Solche hatten wir nämlich auch.“</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Verharmlosung-durch-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Verharmlosung durch Faktenchecker</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Mit Totraum ist jener Bereich des Atemtraktes gemeint, der naturgemäss nicht direkt am Gasaustausch teilnimmt, der also nicht ständig mit sauerstoffreicher Luft versorgt wird. Vergrössert sich dieser Totraum aufgrund von Schädigungen der Lungen beispielsweise oder auch durch Atemprobleme bzw. dem erschwerten Atmen durch eine Maske, dann kann dies gerade bei Kindern und insbesondere bei FFP2-Masken zu Beschwerden wie Kopfschmerzen führen.</p>
                    <p>Der vergrösserte Totraum wird vom Mainstream bzw. den üblichen Faktencheckern und den von ihnen befragten Experten als gering eingestuft. Der Körper könne das problemlos durch verstärkte Atemarbeit kompensieren, heisst es. Trage man die Maske längere Zeit (was ja bei Kindern in der Schule der Fall ist), dann sei das halt anstrengend und daher bekomme man Kopfschmerzen, Gesundheitsschäden seien dadurch aber nicht zu befürchten (5). Selbst wenn alles so harmlos sein sollte, fragt man sich, ob diese Experten schon einmal versucht haben, sich mit Kopfschmerzen und Müdigkeit zu konzentrieren, zu lernen und aktiv und mit Freude am Unterricht teilzunehmen.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Kein-Unterschied-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Kein Unterschied zwischen OP-Masken und FFP2-Masken </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Zurück zur Untersuchung von Hockertz und Walach. Prof. Walach, der Leiter der Studie, wertete die Daten aus und stellte auch keinen merklichen Unterschied zwischen OP-Masken und FFP2-Masken fest. Bei FFP2-Masken wurde ein Wert von 13.910 ppm gemessen, bei den OP-Masken ein Wert von 13.120 ppm, was immer noch mehr als das 6-Fache des Grenzwertes darstellt, dessen Überschreitung vom Umweltbundesamt als Gesundheitsgefährdung eingestuft wird. </p>
                    <p>Prof. Walachs logische Schlussfolgerung lautet somit: „Daher wäre es aus unserer Sicht ein politisches und juristisches Gebot der Stunde, das Maskentragen bei Kindern als gesundheitsgefährdend einzustufen und keinesfalls mehr als pauschale Massnahme zu verordnen. Schulen, Ämter und Behörden, die dies verlangen, machen sich aus unserer Sicht der Körperverletzung schuldig.“ </p>
                    <p>Die höchsten Kohlendioxidwerte wurden bei den kleineren Kindern gemessen. Der höchste Wert lag bei 25.000 ppm bei einem 7-Jährigen, was mehr als das 10-Fache des Grenzwertes darstellt. Je älter die Kinder wurden, umso niedriger war der Kohlendioxidgehalt der Einatemluft. Doch lagen die niedrigsten Werte bei einem 15-Jährigen noch immer bei 6000 ppm, dem Dreifachen des Grenzwertes.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Schulen-dürfen-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Schulen dürfen keine Untersuchungen durchführen lassen</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Interessanterweise wollten ursprünglich zwei Schulen im Landkreis Passau (Deutschland) eine solche Studie durchführen lassen, was ihnen aber von ihren jeweiligen Oberschulämtern verboten wurde, so Prof. Hockertz, der hinzufügt: „Wir hoffen sehr, dass durch unsere Daten etwas mehr Vernunft und Sachlichkeit in die Debatte kommt. Denn das Risiko für ein Kind, an COVID-19 zu erkranken ist wesentlich geringer, als einen psychischen oder körperlichen Schaden durch das Tragen der Masken zu erleiden.“</p>

                    <p>Prof. Dr. Hockertz hat auch ein Buch zum Thema publiziert mit dem Titel *<a href="https://www.amazon.de/Generation-Maske-Corona-Angst-Herausforderung/dp/3864458196">Generation Maske; Corona: Angst und Herausforderung</a>. </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Masken-schaden-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Masken schaden mehr, als sie nützen</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Dr. Ronald Weikl, Arzt für Geburtshilfe und Frauenheilkunde in Passau, der zu den Organisatoren und Betreuern der Studie gehört, ist der Meinung: „Es führt kein Weg daran vorbei: Das Tragen von Masken bei Kindern ist eine ungeeignete Methode des Gesundheitsschutzes. Es schadet mehr, als es nutzt.“</p>
                    <p>Weikl ist mit dieser Meinung nicht allein. Im Film Corona-Kinder von Filmemacher Jens-Tibor Homm kommen Experten zu Wort, die den Corona-Massnahmen äusserst schädliche Auswirkungen auf die körperliche und seelische Gesundheit von Kindern und Jugendlichen prognostizieren (4).</p>
                    <p class="fw-bold">Im vorigen Link unter ( <a href="http://corona-kinder-film.de/" class="text-secondary" target="_blank" rel="noopener noreferrer">4</a> ) können Sie den Film Corona-Kinder (45 Minuten) komplett ansehen. </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Unter-16-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Unter 16 Jahren sind Masken für Kinder nicht sinnvoll</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Kinderärztin Dr. med. Michaela Glöckler beispielsweise hält die Maskenpflicht, wie sie derzeit praktiziert wird, als nicht angemessen. Die Tatsache, dass Kinder ansteckend sein könnten, rechtfertige die Massnahmen wie Schul- und Kindergartenschliessungen nicht. Erwachsene stecken sich überdies doppelt so häufig an, sagt sie. Unter 11 Jahren - so seien sich Kinderärzte einig - sollten keinesfalls Masken getragen werden. Hier überwiegen die Schäden den Nutzen bei weitem. Schon allein weil Kinder in diesem Alter gar nicht die Hygiene der Maske gewährleisten können. </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Seelische-Schäden-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Seelische Schäden bei Kindern durch Masken</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Besonders schwer schätzt Dr. Glöckler die seelischen Schäden ein, die Masken und generell der Umgang mit dem Coronavirus bei Kindern verursachen können. Kindern werde ständig - täglich und über Monate hinweg - gesagt, sie würden andere gefährden und sie müssten aufpassen, weil sie selbst auch durch andere gefährdet seien. Man impft Kindern ein, die Welt sei gefährlich, jeden Tag immer sichtbar (durch die allgegenwärtigen Masken). Sie sagt: „Das ist ein kollektives Trauma, das wir der kommenden Generation mit auf den Weg geben und das umso tiefgreifender wirkt, je jünger die Kinder sind.“ </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Übertragungsrisiko-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Übertragungsrisiko durch Kinder ist minimal</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Dr. med. Steffen Rabe ist Facharzt für Kinder- und Jugendmedizin in München und Sprecher des Vorstandes Ärzte für individuelle Impfentscheidung e. V. Er sagt zum Thema Masken bei Kindern: „Das Übertragungsrisiko von Kindern untereinander oder das von Kindern auf Lehrer ist minimal und vernachlässigbar. Wenn Übertragungen an Schulen oder Kindergärten stattfanden, dann von Lehrern auf Kinder. Es gibt keine einzige wissenschaftlich hochwertige und belastbare Studie, die bewiesen hätte, dass das Maskentragen von Kindern und Jugendlichen zu einem relevanten Rückgang der Ansteckungsfälle führen würde.“ </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Keine-Maskenpause-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Keine Maskenpause für Kinder </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Die deutschen Vorschriften für Arbeitnehmer lauten überdies im Sinne des Arbeitsschutzes, so Dr. Rabe, dass nach zwei Stunden Maskentragen eine längere Pause nötig ist. Kinder aber tragen ihre Maske in der Schule und auch im Schulhof viele Stunden, ohne dass sich jemand über die Notwendigkeit einer Maskenpause Gedanken machen würde.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Masken-verhindern-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Masken verhindern Entwicklung des Atemtrakts bei Kindern </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Prof. Dr. med. Christian Schubert, Arzt, Psychologe und Psychotherapeut gibt zu bedenken: </p>
                    <p>„Wenn Masken getragen werden, dann kann sich der Atemtrakt nicht so entwickeln, wie er es ohne Maske tun würde, die Schleimhautausbildung, die Auseinandersetzung mit Mikroorganismen oder die naturgemässe Besiedlung mit Bakterien und Pilzen finden nicht statt. Dazu kommt ein zu hoher Kohlendioxidgehalt im Atemtrakt und demzufolge ein erhöhter Partialdruck in den Arterien, so dass man von Schädigungen ausgehen muss.“</p>
                    <p>Wenn der Partialdruck zu hoch ist, bedeutet das, dass von einem Gas (in diesem Fall Kohlendioxid) zu viel im Blut und den Arterien enthalten ist. Man spricht dann von einer Hyperkapnie, die zu geröteter Haut und im Extremfall zu Herzrhythmusstörungen, Krampfanfällen bis hin zu Bewusstlosigkeit führen kann.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Social-Distancing-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Social Distancing und Angst schwächen das Immunsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>„Social Distancing ist eine Art Sterilisierung des Lebens“, sagt Dr. Schubert weiter. „Man weiss, dass zu viel Hygiene auf der rein immunologischen Ebene Krankheit schafft. Dazu kommt, dass Angst und Panik das Immunsystem langfristig schwächen, es kommt zu einer regelrechten Suppression des Immunsystems. Angst und Panik schwächen das Immunsystem ausserdem genau in dem Bereich, der zum Schutz von Viren nötig wäre. Es gibt weniger natürliche Killerzellen, weniger Lymphozyten und andere Zellfraktionen, die wir in der Krise dringend brauchen, um gesund zu bleiben.“ </p>
                    <p>Auch Dr. Rabe warnt in diesem Zusammenhang: „Wir nehmen Kindern und ihrem Immunsystem die Möglichkeit, sich in einer normalen Umwelt zu entwickeln. Die üblichen Infekte finden nicht mehr statt, wären aber nötig für die Reifung eines gesunden Immunsystems.“ Wenn Sie sich für den Schutz Ihrer Kinder einsetzen möchten, nehmen Sie Kontakt auf mit der Elterninitiative</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Expert Modals  -->
    <div class="modal fade" id="Michaela-Glöckler-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Michaela Glöckler</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Kinderärztin. Von 1988 bis 2016 Leitung der Medizinischen Sektion am Goetheanum/Schweiz, Mitbegründerin der Alliance for Childhood und der Europäischen Allianz von Initiativen angewandter Anthroposophie/ELIANT. Internationale Vortrags- und Seminartätigkeit, zahlreiche Publikationen.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Steffen-Rabe-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Dr. med. Steffen Rabe </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Facharzt für Kinder- und Jugendmedizin in München und Sprecher des Vorstandes ‚Ärzte für individuelle Impfentscheidung‘ e. V.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Christian-Schubert-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Prof. Dr. med. Dr. rer. nat. M. Sc. Christian Schubert</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Arzt, Psychologe, Psychotherapeut. Leiter des Labors für Psychoneuroimmunologie an der Klinik für Medizinische Psychologie der Medizinischen Universität Innsbruck. Leiter der Arbeitsgruppe „Psychoneuroimmunologie“ des Deutschen Kollegiums für Psychosomatische Medizin (DKPM). Vorstandsmitglied der Thure von Uexküll-Akademie für Integrierte Medizin (AIM). Arbeitsschwerpunkte: Entwicklung eines integrativen Ansatzes zur Erforschung psychosomatischer Komplexität, kombinierter Einsatz von qualitativen Methoden und Zeitreihenanalyse in der Psychoneuroimmunologie; Medizinphilosophie; Systemtheorie; Psychodynamische Psychotherapie.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Franz-Ruppert-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Prof. Dr. Franz Ruppert</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Diplom-Psychologe, Psychologischer Psychotherapeut, ist Professor an der Katholischen Stiftungshochschule München und in eigener Praxis tätig. Seit 1994 führt er in Deutschland und im europäischen Ausland Workshops durch zu der von ihm entwickelten Methode »Aufstellung des Anliegens«. Er ist spezialisiert auf die psychotherapeutische Arbeit mit schweren psychischen Erkrankungen wie Depressionen, Ängsten, Borderline-Persönlichkeitsstörungen, Psychosen und Schizophrenien. Dazu kommen zahlreiche Veröffentlichungen zu den Themen Symbiosetrauma und Aufstellungsarbeit. </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Christian-Prestien-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Hans-Christian Prestien</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Hans-Christian war ab 1977 Familienrichter „der ersten Stunde“. Bis 2009 sammelte er als Familien- und Jugendrichter, Rechtsanwalt und nach der Wende erneut als Familienrichter Erfahrungen im Umgang mit Kindern, Jugendlichen und ihren Familien, die in persönliche oder familiäre Notlagen geraten waren. Als vorlegender Richter und Sachverständiger für den Deutschen Kinderschutzbund war Hans-Christian Prestien massgeblich an der Entscheidung des Bundesverfassungsgerichts zur fortdauernden gemeinsamen Sorge nach Scheidung der Eltern beteiligt.</p>
                    <p>Im Interesse einer psychologisch und pädagogisch an den jeweiligen Kindesbedürfnissen ausgerichteten Arbeitsweise der MitarbeiterInnen in Justiz und Jugendämtern hat Hans-Christian Prestien bereits in den frühen achtziger Jahren als Vorstandsmitglied des Deutschen Kinderschutzbundes eine interdisziplinäre, unabhängige „Anwaltschaft des Kindes“ konzipiert. Zur deren Realisierung ist er seit 1983 an vielfältigen Fachtagungen und Weiterbildungen beteiligt.</p>
                    <p>Zur Situation des Kindschaftsrechts hat er mehrere Beiträge veröffentlicht, u.a. in der Festschrift für J. Rudolph: „Verändertes Denken – zum Wohle der Kinder“, Nomos Vlg 2009, und in „Scheiden tut weh“ von A. Karger und M. Franz, V & R Verlag 2013 Seit seiner Pensionierung bietet er zusammen mit seiner Frau Seminare zur Qualifizierung von Verfahrensbeiständen und anderen Professionen des Kindschaftsrechts an. Auch bietet er allen von Beziehungsbrüchen betroffenen Erwachsenen die Möglichkeit zur Beratung und Unterstützung auf dem Weg zum Frieden.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Christian-KreiB-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Prof. Dr. Christian Kreiß</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Studium und Promotion in Volkswirtschaftslehre und Wirtschaftsgeschichte an der LMU München. Neun Jahre Berufstätigkeit als Bankier, davon sieben Jahre als Investment Banker. Seit 2002 Professor an der Hochschule Aalen für Finanzierung und Volkswirtschaftslehre. Autor von sieben Büchern: Gekaufte Wissenschaft (2020); Das Mephisto Prinzip in unserer Wirtschaft (2019); BWL-Blenden Wuchern Lamentieren (2019, zusammen mit Heinz Siebenbrock); Werbung nein danke (2016); Gekaufte Forschung (2015); Geplanter Verschleiss (2014); Profitwahn (2013). Drei Einladungen in den Deutschen Bundestag als unabhängiger Experte (Grüne, Linke, SPD), Gewerkschaftsmitglied bei ver.di. Zahlreiche Fernseh-, Rundfunk- und Zeitschriften Interviews, öffentliche Vorträge und Veröffentlichungen.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Leonard-Heffels-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content text-primary">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">Leonard Heffels</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Lehrer und Schriftsteller, studierte Kunst in Maastricht und Pädagogik in Amsterdam. In seinem literarischen Werk setzt er sich immer wieder mit biblischen Themen auseinander. Dabei bewegt er sich im Grenzbereich zwischen Lyrik und Prosa, so zum Beispiel in „Wer mit Gott geht …“ und „Volkes König“. Auch seine Novelle „Marthas Geschick“ ist geprägt von einem lyrischen Sprachstil, der eine grosse atmosphärische Dichte schafft. Zugleich wirft er ein neues Licht auf die Protagonisten, die einfühlsam und tiefsinnig dargestellt werden. Bei TWENTYSIX erschienen ferner der historische Roman „Daniels Vermächtnis“, „Dinahs Ehre“ und der unkonventionelle Glaubensroman „Sieben“. Unter dem Pseudonym Nerodal Feh Fesl veröffentlichte er den zweiteiligen Roman „Die Vorbotin“.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Payment Modal  -->
    <div class="modal fade" id="paymentModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content text-primary p-2">
                <div class="modal-header">
                    <h5 class="modal-title h4 fw-bold">SUPPORT NOW</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form name="my_form" method="POST" action="<?php $_SERVER['PHP_SELF']; ?>" class="row mx-0 g-3 needs-validation my_form" novalidate>
                    <div class="col-12 modal-body">
                        <div class="mb-3">
                            <label class="d-block form-label">Anrede <sup class="text-danger">*</sup></label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="anrede" id="Frau" value="Frau">
                                <label class="form-check-label" for="Frau">Frau</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="anrede" id="Herr" value="Herr">
                                <label class="form-check-label" for="Herr">Herr</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="Vorname" class="form-label">Vorname <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" id="Vorname" name="vorname" placeholder="Vorname" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="Nachname" class="form-label">Nachname <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" id="Nachname" name="nachname" placeholder="Nachname" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="checkbox" value="" id="Firmen-oder" onclick="firmen()">
                                    <label class="form-check-label" for="Firmen-oder">Firmen- oder Organisationsspende</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div id="Firmen-oder-text" class="mb-3" style="display: none;">
                                    <label for="Organisation" class="form-label">Firma / Organisation</label>
                                    <input type="text" class="form-control" name="firma_organisation" id="Organisation" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="E-Mail" class="form-label">E-Mail <sup class="text-danger">*</sup></label>
                                    <input type="email" class="form-control" name="email" id="E-Mail" placeholder="abc@xyz.com" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="Telefon" class="form-label">Telefon</label>
                                    <input type="text" class="form-control numbers" name="telefon" id="Telefon" placeholder="9876543210" maxlength="12">
                                </div>
                            </div>
                        </div>
                        <h4 class="h3">Adresse</h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="mb-3">
                                    <label for="Land" class="form-label">Land</label>
                                    <select id="Land" class="form-select" name="land">
                                        <option value="AF">Afghanistan</option>
                                        <option value="EG">Ägypten</option>
                                        <option value="AX">Ålandinseln</option>
                                        <option value="AL">Albanien</option>
                                        <option value="DZ">Algerien</option>
                                        <option value="AS">Amerikanisch-Samoa</option>
                                        <option value="VI">Amerikanische Jungferninseln</option>
                                        <option value="UM">Amerikanische Überseeinseln</option>
                                        <option value="AD">Andorra</option>
                                        <option value="AO">Angola</option>
                                        <option value="AI">Anguilla</option>
                                        <option value="AQ">Antarktis</option>
                                        <option value="AG">Antigua und Barbuda</option>
                                        <option value="GQ">Äquatorialguinea</option>
                                        <option value="AR">Argentinien</option>
                                        <option value="AM">Armenien</option>
                                        <option value="AW">Aruba</option>
                                        <option value="AC">Ascension</option>
                                        <option value="AZ">Aserbaidschan</option>
                                        <option value="ET">Äthiopien</option>
                                        <option value="AU">Australien</option>
                                        <option value="BS">Bahamas</option>
                                        <option value="BH">Bahrain</option>
                                        <option value="BD">Bangladesch</option>
                                        <option value="BB">Barbados</option>
                                        <option value="BY">Belarus</option>
                                        <option value="BE">Belgien</option>
                                        <option value="BZ">Belize</option>
                                        <option value="BJ">Benin</option>
                                        <option value="BM">Bermuda</option>
                                        <option value="BT">Bhutan</option>
                                        <option value="BO">Bolivien</option>
                                        <option value="BA">Bosnien und Herzegowina</option>
                                        <option value="BW">Botsuana</option>
                                        <option value="BV">Bouvetinsel</option>
                                        <option value="BR">Brasilien</option>
                                        <option value="VG">Britische Jungferninseln</option>
                                        <option value="IO">Britisches Territorium im Indischen Ozean</option>
                                        <option value="BN">Brunei Darussalam</option>
                                        <option value="BG">Bulgarien</option>
                                        <option value="BF">Burkina Faso</option>
                                        <option value="BI">Burundi</option>
                                        <option value="CV">Cabo Verde</option>
                                        <option value="EA">Ceuta und Melilla</option>
                                        <option value="CL">Chile</option>
                                        <option value="CN">China</option>
                                        <option value="CP">Clipperton-Insel</option>
                                        <option value="CK">Cookinseln</option>
                                        <option value="CR">Costa Rica</option>
                                        <option value="CI">Côte d’Ivoire</option>
                                        <option value="CW">Curaçao</option>
                                        <option value="DK">Dänemark</option>
                                        <option value="DE">Deutschland</option>
                                        <option value="DG">Diego Garcia</option>
                                        <option value="DM">Dominica</option>
                                        <option value="DO">Dominikanische Republik</option>
                                        <option value="DJ">Dschibuti</option>
                                        <option value="EC">Ecuador</option>
                                        <option value="SV">El Salvador</option>
                                        <option value="ER">Eritrea</option>
                                        <option value="EE">Estland</option>
                                        <option value="SZ">Eswatini</option>
                                        <option value="FK">Falklandinseln</option>
                                        <option value="FO">Färöer</option>
                                        <option value="FJ">Fidschi</option>
                                        <option value="FI">Finnland</option>
                                        <option value="FR">Frankreich</option>
                                        <option value="GF">Französisch-Guayana</option>
                                        <option value="PF">Französisch-Polynesien</option>
                                        <option value="TF">Französische Süd- und Antarktisgebiete</option>
                                        <option value="GA">Gabun</option>
                                        <option value="GM">Gambia</option>
                                        <option value="GE">Georgien</option>
                                        <option value="GH">Ghana</option>
                                        <option value="GI">Gibraltar</option>
                                        <option value="GD">Grenada</option>
                                        <option value="GR">Griechenland</option>
                                        <option value="GL">Grönland</option>
                                        <option value="GP">Guadeloupe</option>
                                        <option value="GU">Guam</option>
                                        <option value="GT">Guatemala</option>
                                        <option value="GG">Guernsey</option>
                                        <option value="GN">Guinea</option>
                                        <option value="GW">Guinea-Bissau</option>
                                        <option value="GY">Guyana</option>
                                        <option value="HT">Haiti</option>
                                        <option value="HM">Heard und McDonaldinseln</option>
                                        <option value="HN">Honduras</option>
                                        <option value="IN">Indien</option>
                                        <option value="ID">Indonesien</option>
                                        <option value="IQ">Irak</option>
                                        <option value="IR">Iran</option>
                                        <option value="IE">Irland</option>
                                        <option value="IS">Island</option>
                                        <option value="IM">Isle of Man</option>
                                        <option value="IL">Israel</option>
                                        <option value="IT">Italien</option>
                                        <option value="JM">Jamaika</option>
                                        <option value="JP">Japan</option>
                                        <option value="YE">Jemen</option>
                                        <option value="JE">Jersey</option>
                                        <option value="JO">Jordanien</option>
                                        <option value="KY">Kaimaninseln</option>
                                        <option value="KH">Kambodscha</option>
                                        <option value="CM">Kamerun</option>
                                        <option value="CA">Kanada</option>
                                        <option value="IC">Kanarische Inseln</option>
                                        <option value="BQ">Karibische Niederlande</option>
                                        <option value="KZ">Kasachstan</option>
                                        <option value="QA">Katar</option>
                                        <option value="KE">Kenia</option>
                                        <option value="KG">Kirgisistan</option>
                                        <option value="KI">Kiribati</option>
                                        <option value="CC">Kokosinseln</option>
                                        <option value="CO">Kolumbien</option>
                                        <option value="KM">Komoren</option>
                                        <option value="CG">Kongo-Brazzaville</option>
                                        <option value="CD">Kongo-Kinshasa</option>
                                        <option value="XK">Kosovo</option>
                                        <option value="HR">Kroatien</option>
                                        <option value="CU">Kuba</option>
                                        <option value="KW">Kuwait</option>
                                        <option value="LA">Laos</option>
                                        <option value="LS">Lesotho</option>
                                        <option value="LV">Lettland</option>
                                        <option value="LB">Libanon</option>
                                        <option value="LR">Liberia</option>
                                        <option value="LY">Libyen</option>
                                        <option value="LI">Liechtenstein</option>
                                        <option value="LT">Litauen</option>
                                        <option value="LU">Luxemburg</option>
                                        <option value="MG">Madagaskar</option>
                                        <option value="MW">Malawi</option>
                                        <option value="MY">Malaysia</option>
                                        <option value="MV">Malediven</option>
                                        <option value="ML">Mali</option>
                                        <option value="MT">Malta</option>
                                        <option value="MA">Marokko</option>
                                        <option value="MH">Marshallinseln</option>
                                        <option value="MQ">Martinique</option>
                                        <option value="MR">Mauretanien</option>
                                        <option value="MU">Mauritius</option>
                                        <option value="YT">Mayotte</option>
                                        <option value="MX">Mexiko</option>
                                        <option value="FM">Mikronesien</option>
                                        <option value="MC">Monaco</option>
                                        <option value="MN">Mongolei</option>
                                        <option value="ME">Montenegro</option>
                                        <option value="MS">Montserrat</option>
                                        <option value="MZ">Mosambik</option>
                                        <option value="MM">Myanmar</option>
                                        <option value="NA">Namibia</option>
                                        <option value="NR">Nauru</option>
                                        <option value="NP">Nepal</option>
                                        <option value="NC">Neukaledonien</option>
                                        <option value="NZ">Neuseeland</option>
                                        <option value="NI">Nicaragua</option>
                                        <option value="NL">Niederlande</option>
                                        <option value="NE">Niger</option>
                                        <option value="NG">Nigeria</option>
                                        <option value="NU">Niue</option>
                                        <option value="KP">Nordkorea</option>
                                        <option value="MP">Nördliche Marianen</option>
                                        <option value="MK">Nordmazedonien</option>
                                        <option value="NF">Norfolkinsel</option>
                                        <option value="NO">Norwegen</option>
                                        <option value="OM">Oman</option>
                                        <option value="AT">Österreich</option>
                                        <option value="PK">Pakistan</option>
                                        <option value="PS">Palästinensische Autonomiegebiete</option>
                                        <option value="PW">Palau</option>
                                        <option value="PA">Panama</option>
                                        <option value="PG">Papua-Neuguinea</option>
                                        <option value="PY">Paraguay</option>
                                        <option value="PE">Peru</option>
                                        <option value="PH">Philippinen</option>
                                        <option value="PN">Pitcairninseln</option>
                                        <option value="PL">Polen</option>
                                        <option value="PT">Portugal</option>
                                        <option value="PR">Puerto Rico</option>
                                        <option value="MD">Republik Moldau</option>
                                        <option value="RE">Réunion</option>
                                        <option value="RW">Ruanda</option>
                                        <option value="RO">Rumänien</option>
                                        <option value="RU">Russland</option>
                                        <option value="SB">Salomonen</option>
                                        <option value="ZM">Sambia</option>
                                        <option value="WS">Samoa</option>
                                        <option value="SM">San Marino</option>
                                        <option value="ST">São Tomé und Príncipe</option>
                                        <option value="SA">Saudi-Arabien</option>
                                        <option value="SE">Schweden</option>
                                        <option value="CH" selected="selected">Schweiz</option>
                                        <option value="SN">Senegal</option>
                                        <option value="RS">Serbien</option>
                                        <option value="SC">Seychellen</option>
                                        <option value="SL">Sierra Leone</option>
                                        <option value="ZW">Simbabwe</option>
                                        <option value="SG">Singapur</option>
                                        <option value="SX">Sint Maarten</option>
                                        <option value="SK">Slowakei</option>
                                        <option value="SI">Slowenien</option>
                                        <option value="SO">Somalia</option>
                                        <option value="HK">Sonderverwaltungsregion Hongkong</option>
                                        <option value="MO">Sonderverwaltungsregion Macau</option>
                                        <option value="ES">Spanien</option>
                                        <option value="SJ">Spitzbergen und Jan Mayen</option>
                                        <option value="LK">Sri Lanka</option>
                                        <option value="BL">St. Barthélemy</option>
                                        <option value="SH">St. Helena</option>
                                        <option value="KN">St. Kitts und Nevis</option>
                                        <option value="LC">St. Lucia</option>
                                        <option value="MF">St. Martin</option>
                                        <option value="PM">St. Pierre und Miquelon</option>
                                        <option value="VC">St. Vincent und die Grenadinen</option>
                                        <option value="ZA">Südafrika</option>
                                        <option value="SD">Sudan</option>
                                        <option value="GS">Südgeorgien und die Südlichen Sandwichinseln</option>
                                        <option value="KR">Südkorea</option>
                                        <option value="SS">Südsudan</option>
                                        <option value="SR">Suriname</option>
                                        <option value="SY">Syrien</option>
                                        <option value="TJ">Tadschikistan</option>
                                        <option value="TW">Taiwan</option>
                                        <option value="TZ">Tansania</option>
                                        <option value="TH">Thailand</option>
                                        <option value="TL">Timor-Leste</option>
                                        <option value="TG">Togo</option>
                                        <option value="TK">Tokelau</option>
                                        <option value="TO">Tonga</option>
                                        <option value="TT">Trinidad und Tobago</option>
                                        <option value="TA">Tristan da Cunha</option>
                                        <option value="TD">Tschad</option>
                                        <option value="CZ">Tschechien</option>
                                        <option value="TN">Tunesien</option>
                                        <option value="TR">Türkei</option>
                                        <option value="TM">Turkmenistan</option>
                                        <option value="TC">Turks- und Caicosinseln</option>
                                        <option value="TV">Tuvalu</option>
                                        <option value="UG">Uganda</option>
                                        <option value="UA">Ukraine</option>
                                        <option value="HU">Ungarn</option>
                                        <option value="UY">Uruguay</option>
                                        <option value="UZ">Usbekistan</option>
                                        <option value="VU">Vanuatu</option>
                                        <option value="VA">Vatikanstadt</option>
                                        <option value="VE">Venezuela</option>
                                        <option value="AE">Vereinigte Arabische Emirate</option>
                                        <option value="US">Vereinigte Staaten</option>
                                        <option value="GB">Vereinigtes Königreich</option>
                                        <option value="VN">Vietnam</option>
                                        <option value="WF">Wallis und Futuna</option>
                                        <option value="CX">Weihnachtsinsel</option>
                                        <option value="EH">Westsahara</option>
                                        <option value="CF">Zentralafrikanische Republik</option>
                                        <option value="CY">Zypern</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="Strasse" class="form-label">Strasse <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" id="Strasse" name="strasse" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-3">
                                    <label for="PLZ" class="form-label">PLZ <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" id="PLZ" name="plz" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="mb-3">
                                    <label for="Ort" class="form-label">Ort <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" id="Ort" name="ort" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="checkbox" value="" id="postfach" onclick="postfachfun()">
                                    <label class="form-check-label" for="postfach">Postfach</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div id="postfach-text" class="mb-3" style="display: none;">
                                    <input type="text" class="form-control" name="postfach" placeholder="">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-check mb-3">
                            <input class="form-check-input" type="checkbox" value="1" id="akzeptiere" name="term_condition" required>
                            <label class="form-check-label" for="akzeptiere">Ja, ich akzeptiere die <a class="text-secondary" href="https://www.unicef.ch/sites/default/files/2021-12/UNICEF_AGB_Spende_2021_DE.pdf" target="_blank" rel="noopener noreferrer">allgemeinen Geschäftsbedingungen</a>.</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
    <script src="./js/bootstrap.bundle.js"></script>
    <script src="./js/jquery.rollNumber.js"></script>
    <script src="./js/custom.js"></script>
    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.numbers').keyup(function() {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });
        });
    </script>
    <?php
    if (isset($_SESSION['success_msg'])) {
    ?>
        <script>
            Swal.fire(
                '<?php echo $_SESSION['success_msg']; ?>',
                '',
                'success'
            )
        </script>
    <?php unset($_SESSION['success_msg']);
    } else if (isset($_SESSION['error_msg'])) { ?>
        <script>
            Swal.fire(
                '<?php echo $_SESSION['error_msg']; ?>',
                '',
                'error'
            )
        </script>
    <?php unset($_SESSION['error_msg']);
    } ?>



</body>

</html>